package wikipedia;

import org.mediawiki.xml.export_0.PageType;
import org.mediawiki.xml.export_0.RevisionType;

import java.util.ArrayList;
import java.util.List;

/**
 * Some utilities for working with wikipedia's XML classes.
 */
public class XmlUtils {
  /**
   * Returns a list of strings containing the the text of the page.
   *
   * Every entry is the text for a different revision.
   *
   * @param page
   * @return
   */
  public static List<String> getPageText(PageType page) {
    List<String> text = new ArrayList<String>();
    for (Object o : page.getRevisionOrUpload()) {
      if (!(o instanceof RevisionType)) {
        continue;
      }
      RevisionType r = (RevisionType) o;
      text.add(r.getText().getValue());
    }
    return text;
  }
}

