package wikipedia;

import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.common.base.Joiner;

import org.mediawiki.xml.export_0.PageType;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Extract the company infoboxes from the page.
 * An info box looks like.
 * {{Infobox company
   | name             = Audi AG&lt;ref name=artinc /&gt;
   | logo             = [[File:Audi AG logo.png|185px]]
   | caption          =
   | image            = Audi Ingolstadt.jpg
   | image_caption    = Audi head office in [[Ingolstadt]]
   | type             = [[Public company|Public]], [[Aktiengesellschaft]]
   | traded_as        = {{FWB|NSU}}
   | fate             =
   | predecessor      = [[Auto Union|Auto Union AG]]&lt;br&gt;[[NSU Motorenwerke|NSU Motorenwerke AG]]
   | successor        =
   ..
   }}
 */
public class ExtractCompanyInfoboxDo extends DoFn<PageType, InfoBox> {
  @Override
  public void processElement(ProcessContext c) {
    PageType page = c.element();

    List<String> revisions = XmlUtils.getPageText(page);
    if (revisions.isEmpty()) {
      return;
    }

    // Only consider the first revision
    // TODO(jlewi): How can we make sure that we are dealing with the latest
    // revision when there multiple revisions.
    String pageText = revisions.get(0);
    String[] lines = pageText.split("\n");
    int index = 0;
    Joiner joiner = Joiner.on("\n");
    while (index < lines.length) {
      if (!lines[index].contains("Infobox company")) {
        index++;
        continue;
      }
      ArrayList<String> infobox = new ArrayList<String>();
      infobox.add(lines[index]);
      index++;
      while(index < lines.length) {
        String line = lines[index].trim();
        infobox.add(line);
        index++;
        if (line.startsWith("}}")) {

          c.output(new InfoBox(infobox));
          break;
        }
      }
    }
  }
}


