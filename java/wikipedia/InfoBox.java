package wikipedia;

import com.google.cloud.dataflow.sdk.coders.AvroCoder;
import com.google.cloud.dataflow.sdk.coders.DefaultCoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * A class for representing an infobox.
 */
@DefaultCoder(AvroCoder.class)
public class InfoBox {
  public static Pattern FIELD_PATTERN = Pattern.compile("\\|([^=]*)=.*");
  public static Pattern LINE_PATTERN = Pattern.compile("\\|([^=]*)=(.*)");

  private List<String> lines;

  public InfoBox() {
    lines = new ArrayList<String>();
  }

  public InfoBox(List<String> lines) {
    this.lines = lines;
  }

  public List<String> getLines() {
    return lines;
  }

  /**
   * Extract the field name if there is one from the line.
   *
   * @param line
   * @return The field name if there is one and the empty string otherwise.
   */
  static public String parseFieldName(String line) {
    Matcher m = FIELD_PATTERN.matcher(line);
    if (!m.matches()) {
      return "";
    }

    return m.group(1).trim();
  }

  /**
   * Split the lines into key value pairs.
   * @return
   */
  public Map<String, String> splitLines() {
    Map<String, String> result = new HashMap<String, String>();
    for (String l : lines) {
      Matcher m = LINE_PATTERN.matcher(l);
      if (!m.matches()) {
        continue;
      }
      result.put(m.group(1).trim(), m.group(2).trim());
    }
    return result;
  }
}
