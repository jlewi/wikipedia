package wikipedia;

import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.CompressedSource;
import com.google.cloud.dataflow.sdk.io.Read;
import com.google.cloud.dataflow.sdk.io.XmlSource;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.PCollection;

import org.mediawiki.xml.export_0.PageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * A Dataflow for building a BigQuery table containing the information in Company infoboxes.
 */
public class BuildCompaniesTable {
  private static final Logger LOG = LoggerFactory.getLogger(BuildCompaniesTable.class);

  /**
   * Convert an infobox to a BigQuery row.
   */
  public static class InfoBoxToBQRowDo extends DoFn<InfoBox, TableRow> {
    private InfoBoxSchema schema;

    public InfoBoxToBQRowDo(InfoBoxSchema schema) {
      this.schema = schema;
    }

    @Override
    public void processElement(ProcessContext c) {
      InfoBox infobox = c.element();
      Map<String, String> fields = infobox.splitLines();
      if (fields.isEmpty()) {
        return;
      }

      TableRow row = new TableRow();
      for (String column : schema.getFields()) {
        String value = "";
        if (fields.containsKey(column)) {
         value = fields.get(column);
        }
        row.set(column, value);
      }
      c.output(row);
    }
  }

  /**
   * Options supported by {@link ExtractCompanyFields}.
   */
  public static interface BuildCompaniesTableOptions extends PipelineOptions {
    @Description("Path of the file to read from")
    String getInput();
    void setInput(String value);

    @Description("Path of the output table to write to")
    String getOutputTable();
    void setOutputTable(String value);
  }

  /**
   * Build pipeline that converts the company info boxes to BigQuery rows.
   *
   * @param <String>
   * @throws IOException
   */
  public static PCollection<TableRow> buildPipeline(PCollection<PageType> pages, InfoBoxSchema schema) throws IOException {
    return pages.apply(ParDo.of(new ExtractCompanyInfoboxDo()))
            .apply(ParDo.of(new InfoBoxToBQRowDo(schema)));
  }

  public static void main(String[] args) throws IOException {
    BuildCompaniesTableOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
      .as(BuildCompaniesTableOptions.class);
    Pipeline p = Pipeline.create(options);

    XmlSource<PageType> xmlSource = XmlSource.<PageType>from(options.getInput())
        .withRootElement("mediawiki")
        .withRecordElement("page")
        .withRecordClass(PageType.class);


    PCollection<PageType> pages = null;

    if (options.getInput().endsWith("bz2")) {
      CompressedSource<PageType> compressedSource = CompressedSource.from(xmlSource).withDecompression(CompressedSource.CompressionMode.BZIP2);
      LOG.info("Assuming input is bzip2 compressed.");
      pages = p.apply(Read.from(compressedSource));
    } else {
      LOG.info("Assuming input is uncompresed.");
      pages = p.apply(Read.from(xmlSource));
    }

    InfoBoxSchema infoboxSchema = InfoBoxSchema.LodFromResources();
    PCollection<TableRow> fields = buildPipeline(pages, infoboxSchema);


    fields.apply(BigQueryIO.Write
        .named("WriteToBQ")
        .to(options.getOutputTable())
        .withSchema(infoboxSchema.toBigQuerySchema())
        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE));

    p.run();
  }
}
