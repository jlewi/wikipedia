package wikipedia;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.CompressedSource;
import com.google.cloud.dataflow.sdk.io.Read;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.io.XmlSource;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.RemoveDuplicates;
import com.google.cloud.dataflow.sdk.values.PCollection;

import org.mediawiki.xml.export_0.PageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Dataflow for generating a list of the fields found in Company infoboxes.
 */
public class ExtractCompanyFields {
  private static final Logger LOG = LoggerFactory.getLogger(ExtractCompanyFields.class);

  /**
   * Identify the names of the keys of the info box fields.
   */
  public static class ExtractFieldNamesDo extends DoFn<InfoBox, String> {
    @Override
    public void processElement(ProcessContext c) {
      InfoBox infobox = c.element();
      for (String l : infobox.getLines()) {
        String name = InfoBox.parseFieldName(l);
        if (!name.isEmpty()) {
           c.output(name);
        }
      }
    }
  }

  /**
   * Options supported by {@link ExtractCompanyFields}.
   */
  public static interface ExtractCompanyFieldsOptions extends PipelineOptions {
    @Description("Path of the file to read from")
    String getInput();
    void setInput(String value);

    @Description("Path of the file to write to")
    String getOutput();
    void setOutput(String value);
  }

  /**
   * Build pipeline that extracts the field names from the pages.
   * @param <String>
   */
  public static PCollection<String> buildPipeline(PCollection<PageType> pages) {
    return pages.apply(ParDo.of(new ExtractCompanyInfoboxDo()))
            .apply(ParDo.of(new ExtractFieldNamesDo()))
            .apply(RemoveDuplicates.<String>create());
  }

  public static void main(String[] args) {
    ExtractCompanyFieldsOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
      .as(ExtractCompanyFieldsOptions.class);
    Pipeline p = Pipeline.create(options);

    XmlSource<PageType> xmlSource = XmlSource.<PageType>from(options.getInput())
        .withRootElement("mediawiki")
        .withRecordElement("page")
        .withRecordClass(PageType.class);


    PCollection<PageType> pages = null;

    if (options.getInput().endsWith("bz2")) {
      CompressedSource<PageType> compressedSource = CompressedSource.from(xmlSource).withDecompression(CompressedSource.CompressionMode.BZIP2);
      LOG.info("Assuming input is bzip2 compressed.");
      pages = p.apply(Read.from(compressedSource));
    } else {
      LOG.info("Assuming input is uncompresed.");
      pages = p.apply(Read.from(xmlSource));
    }

    PCollection<String> fields = buildPipeline(pages);
    fields.apply(TextIO.Write.to(options.getOutput()));

    p.run();
  }
}
