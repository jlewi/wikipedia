package wikipedia;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.common.io.Resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for representing information about the fields found in infoboxes.
 */
public class InfoBoxSchema implements Serializable {
  private static final Logger LOG = LoggerFactory.getLogger(InfoBoxSchema.class);

  // BigQuery Fields must contain only letters, numbers, and underscores, start
  // with a letter or underscore, and be at most 128 characters long.
  public static Pattern VALID_BIGQUER_FIELD_NAME = Pattern.compile(
      "[a-zA-Z_]{1}[\\w_]{0,127}");

  private List<String> fields;

  public InfoBoxSchema(List<String> fields) {
    this.fields = new ArrayList<String>();
    for (String f : fields) {
      Matcher m = VALID_BIGQUER_FIELD_NAME.matcher(f);
      if (!m.matches()) {
        LOG.warn("Dropping field {} because it isn't a valid BigQuery field name.", f);
        continue;
      }
      this.fields.add(f);
    }
  }

  public static InfoBoxSchema LoadFromFile(String filePath) {
      return new InfoBoxSchema(new ArrayList<String>());
  }

  public static InfoBoxSchema LodFromResources() throws IOException {
    List<String> names = Resources.readLines(Resources.getResource("wikipedia/infobox_field_names.txt"), Charset.defaultCharset());
    return new InfoBoxSchema(names);
  }

  public List<String> getFields() {
    // TODO(jlewi): Would it be better to use an immutable list.
    return Collections.unmodifiableList(fields);
  }

  public TableSchema toBigQuerySchema() {
    List<TableFieldSchema> tableFields = new ArrayList<>();
    for (String f : fields) {
      TableFieldSchema fieldSchema = new TableFieldSchema();
      fieldSchema.setName(f);
      fieldSchema.setType("STRING");
      tableFields.add(fieldSchema);
    }
    TableSchema schema = new TableSchema().setFields(tableFields);
    return schema;
  }
}
