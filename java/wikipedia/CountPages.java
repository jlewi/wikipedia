package wikipedia;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.CompressedSource;
import com.google.cloud.dataflow.sdk.io.Read;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.io.XmlSource;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.Count;
import com.google.cloud.dataflow.sdk.transforms.MapElements;
import com.google.cloud.dataflow.sdk.transforms.SimpleFunction;
import com.google.cloud.dataflow.sdk.values.PCollection;

import org.mediawiki.xml.export_0.PageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * A simple Dataflow job to count wikipedia pages in a dataset.
 */
public class CountPages {
  private static final Logger LOG = LoggerFactory.getLogger(CountPages.class);

  /**
   * Options supported by {@link CountPages}.
   */
  public static interface CountPagesOptions extends PipelineOptions {
    @Description("Path of the file to read from")
    String getInput();
    void setInput(String value);

    @Description("Path of the output table to write to")
    String getOutput();
    void setOutput(String value);
  }

  public static class FormatAsTextFn extends SimpleFunction<Long, String> {
    @Override
    public String apply(Long input) {
      return "Number of pages: " + input;
    }
  }

  public static void main(String[] args) throws IOException {
    CountPagesOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
      .as(CountPagesOptions.class);
    Pipeline p = Pipeline.create(options);

    XmlSource<PageType> xmlSource = XmlSource.<PageType>from(options.getInput())
        .withRootElement("mediawiki")
        .withRecordElement("page")
        .withRecordClass(PageType.class);

    PCollection<PageType> pages = null;

    if (options.getInput().endsWith("bz2")) {
      CompressedSource<PageType> compressedSource = CompressedSource.from(xmlSource).withDecompression(CompressedSource.CompressionMode.BZIP2);
      LOG.info("Assuming input is bzip2 compressed.");
      pages = p.apply(Read.from(compressedSource));
    } else {
      LOG.info("Assuming input is uncompresed.");
      pages = p.apply(Read.from(xmlSource));
    }

    pages.apply(Count.<PageType>globally())
    .apply(MapElements.via(new FormatAsTextFn()))
    .apply(TextIO.Write.named("WriteCounts").to(options.getOutput()));

    p.run();
  }
}


