package wikipedia;

import static org.junit.Assert.assertEquals;

import com.google.cloud.dataflow.sdk.transforms.DoFnTester;
import com.google.common.base.Joiner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mediawiki.xml.export_0.PageType;
import org.mediawiki.xml.export_0.RevisionType;
import org.mediawiki.xml.export_0.TextType;

import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class ExtractCompanyInfoBoxDoTest {
  @Test
  public void testDo() {
    ExtractCompanyInfoboxDo fn = new ExtractCompanyInfoboxDo();

     ArrayList<String> infolines = new ArrayList<String>();

     infolines.add("Infobox company");
     infolines.add("| name             = Audi AG&lt;ref name=artinc /&gt;");
     infolines.add("| logo             = [[File:Audi AG logo.png|185px]]");
     infolines.add("| caption          =");
     infolines.add("| image            = Audi Ingolstadt.jpg");
     infolines.add("| image_caption    = Audi head office in [[Ingolstadt]]");
     infolines.add("}}");

     ArrayList<String> lines = new ArrayList<String>();
     lines.add("<format>text/x-wiki</format>");
     lines.add("<text xml:space=\"preserve\">{{pp-vandalism|small=yes}}");
     lines.add("){{Use dmy dates|date=March 2013}}");
     lines.addAll(infolines);
     lines.add("Audi has been a majority owned");
     Joiner joiner = Joiner.on("\n");
     PageType testInput = new PageType();
     RevisionType revision = new RevisionType();
     TextType revisionText = new TextType();
     revisionText.setValue(joiner.join(lines));
     revision.setText(revisionText);
     testInput.getRevisionOrUpload().add(revision);
     DoFnTester<PageType, InfoBox> fnTester = DoFnTester.of(fn);
     List<InfoBox> testOutputs = fnTester.processBatch(testInput);
     assertEquals(1, testOutputs.size());
     assertEquals(infolines, testOutputs.get(0).getLines());
  }
}

