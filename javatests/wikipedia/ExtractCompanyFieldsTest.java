package wikipedia;

import static org.junit.Assert.assertEquals;

import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.Read;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.io.XmlSource;
import com.google.cloud.dataflow.sdk.testing.TestPipeline;
import com.google.cloud.dataflow.sdk.transforms.DoFnTester;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.common.io.Files;
import com.google.testing.util.TestUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mediawiki.xml.export_0.PageType;

import wikipedia.ExtractCompanyFields.ExtractFieldNamesDo;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public final class ExtractCompanyFieldsTest {
  @Test
  public void testExtractFieldNamesDoFn() {
    ExtractFieldNamesDo fn = new ExtractFieldNamesDo();

   ArrayList<String> lines = new ArrayList<String>();

   lines.add("Infobox company");
   lines.add("| name             = Audi AG&lt;ref name=artinc /&gt;");
   lines.add("| logo             = [[File:Audi AG logo.png|185px]]");
   lines.add("| caption          =");
   lines.add("| image            = Audi Ingolstadt.jpg");
   lines.add("| image_caption    = Audi head office in [[Ingolstadt]]");
   lines.add("}}");

   InfoBox testInput = new InfoBox(lines);
   DoFnTester<InfoBox, String> fnTester = DoFnTester.of(fn);
   List<String> testOutputs = fnTester.processBatch(testInput);
   Assert.assertThat(testOutputs,
                    JUnitMatchers.hasItems("name", "logo", "caption", "image", "image_caption"));
  }

  @Rule
  public TemporaryFolder outputFolder = new TemporaryFolder();

  @Test
  public void testPipeline() throws IOException {
    // Test the complete pipeline.
    String dataFile = TestUtil.getSrcDir()
        + "/google3/experimental/users/jlewi/wikipedia/javatests/wikipedia/testdata/page_sample.xml";


    Pipeline p = TestPipeline.create();

    XmlSource<PageType> source = XmlSource.<PageType>from(dataFile)
        .withRootElement("mediawiki")
        .withRecordElement("page")
        .withRecordClass(PageType.class);

    PCollection<PageType> pages = p.apply(Read.from(source));
    PCollection<String> fieldNames = ExtractCompanyFields.buildPipeline(pages);

    File outDir = outputFolder.newFolder("output");
    File outShardedPath = new File(outDir, "field_names");
    fieldNames.apply(TextIO.Write.to(outShardedPath.getAbsolutePath()));

    p.run();

    // Read the output files.
    PrefixFileFilter fileFilter = new PrefixFileFilter("field_names");
    Collection<File> outputFiles = FileUtils.listFiles(outDir, fileFilter, null);

    List<String> actualNames = new ArrayList<String>();
    for (File o : outputFiles) {
      List<String> lines = Files.readLines(o, Charset.defaultCharset());
      actualNames.addAll(lines);
    }
    Collections.sort(actualNames);

    List<String> expected = Arrays.asList("assets", "caption", "defunct", "divisions", "equity", "fate", "footnotes", "foundation", "founded", "founder", "founders", "homepage", "image", "image_caption", "industry", "intl", "key_people", "location_city", "location_country", "locations", "logo", "name", "net_income", "num_employees", "operating_income", "owner", "parent", "predecessor", "production", "products", "revenue", "services", "slogan", "subsid", "successor", "traded_as", "type");
    assertEquals(expected, actualNames);
  }
}
