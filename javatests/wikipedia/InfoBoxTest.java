package wikipedia;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class InfoBoxTest {
  @Test
  public void testParseFieldName() {
    String line = "| foundation       = [[Chemnitz]], Germany&lt;br /&gt;({{Start date and age|df=yes|1932|06|29}})&lt;ref name=&quot;Audi history&quot;&gt;{{cite web|title=History of Audi AG|url=http://www.audi.com/com/brand/en/company/audi_history/personalities/august_horch.html}}&lt;/ref&gt;jj";
    assertEquals(
        "foundation", InfoBox.parseFieldName(line));
  }

  @Test
  public void testParseFieldNameNoMatch() {
    String line = "}}";
    assertEquals("", InfoBox.parseFieldName(line));
  }

  @Test
  public void testSplitLines() {
    ArrayList<String> infolines = new ArrayList<String>();

    infolines.add("Infobox company");
    infolines.add("| name             = Audi AG&lt;ref name=artinc /&gt;");
    infolines.add("| logo             = [[File:Audi AG logo.png|185px]]");
    infolines.add("| caption          =");
    infolines.add("| image            = Audi Ingolstadt.jpg");
    infolines.add("| image_caption    = Audi head office in [[Ingolstadt]]");
    infolines.add("}}");

    InfoBox infobox = new InfoBox(infolines);
    Map<String, String> actual = infobox.splitLines();

    HashMap<String, String> expected = new HashMap<String, String>();
    expected.put("name", "Audi AG&lt;ref name=artinc /&gt;");
    expected.put("logo", "[[File:Audi AG logo.png|185px]]");
    expected.put("caption", "");
    expected.put("image"            , "Audi Ingolstadt.jpg");
    expected.put("image_caption"    , "Audi head office in [[Ingolstadt]]");

    assertEquals(expected, actual);
  }
}

